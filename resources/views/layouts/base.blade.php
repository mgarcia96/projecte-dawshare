<!DOCTYPE html>
<html lang="">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>DawShare</title>

    <!-- Material Design fonts -->
     <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet"> 
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/icon?family=Material+Icons">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    <!-- Bootstrap Material Design -->
    <link rel="stylesheet" href="{{ asset('node_modules/bootstrap-material-design/dist/css/bootstrap-material-design.min.css') }}">
    <link rel="stylesheet" href="{{ asset('node_modules/bootstrap-material-design/dist/css/ripples.min.css') }}">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.css">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css?v='.time()) }}">

    @yield('css')

  </head>
  <body>
    <div class="navbar navbar-inverse">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="{{ url('/') }}">DawShare</a>
        </div>
        <div class="navbar-collapse collapse navbar-responsive-collapse">
          <ul class="nav navbar-nav">
          </ul>

          <ul class="nav navbar-nav navbar-right">

            {{-- PROFILE --}}
            <li class="dropdown">
              <a href="http://fezvrasta.github.io/bootstrap-material-design/bootstrap-elements.html" data-target="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="{{ asset(Auth::user()->profile_image ? 'img/users/'.Auth::user()->profile_image : 'img/no-profile.png') }}" class="img-responsive img-top-profile" alt="">
              {{ Auth::user()->name }}
                <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="{{ url('/user/'.Auth::user()->url) }}">Perfil</a></li>
                <li class="divider"></li>
                <li><a href="{{ url('/auth/logout') }}">Desconectar</a></li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </div>

    <section class="container">
      <article class="row">
        <div class="col-md-12">
          
          @yield('content')

        </div>
      </article>
    </section>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

    <!-- Material Design for Bootstrap -->
    <script src="{{ asset('node_modules/bootstrap-material-design/dist/js/material.min.js') }}"></script>
    <script src="{{ asset('node_modules/bootstrap-material-design/dist/js/ripples.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.js"></script>
    <script>
        $.material.init();

        $(function() {

          @if(Session::has('msg'))
            swal("{{ Session::get('msg') }}", "", "{{ Session::get('type') }}");
          @endif

        });
    </script>

    @yield('js')

  </body>
</html>