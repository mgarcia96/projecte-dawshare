<div class="col-md-4 sidebar-profile-box">

	{{-- PROFILE BOX --}}
	<div class="well">
		<div class="profile">
			<img src="{{ asset('/img/users/'.$user->profile_image) }}" alt="" class="img-responsive profile_image">

			<div class="page-header text-center">
				<h4>{{ $user->name }}</h4>
			</div>

			<p class="text-info">
				<b>{{ $user->countPosts() }}</b> Posts publicats
			</p>

			@if(Auth::user()->url != request()->segment(2))
				@if(isset(Auth::user()->friends) && in_array($user->url, Auth::user()->friends))
					<button class="btn btn-warning btn-raised btn-block" id="remove-friend"><i class="fa fa-thumbs-down"></i> Borrar dels meus amics</button>
				@else
					<button class="btn btn-info btn-raised btn-block" id="add-friend"><i class="fa fa-thumbs-up"></i> Afegir com a amic</button>
				@endif
			@endif

		</div>
	</div>

</div>