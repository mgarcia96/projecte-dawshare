<div class="col-md-4 sidebar-profile-box">

	{{-- PROFILE BOX --}}
	<div class="well">
		<div class="profile">
			<img src="{{ asset('/img/users/'.Auth::user()->profile_image) }}" alt="" class="img-responsive profile_image">

			<div class="page-header text-center">
				<h4>{{ Auth::user()->name }}</h4>
			</div>

			<a class="text-info" href="{{ url('/user/'.Auth::user()->url) }}"><i class="fa fa-edit"></i> Editar perfil</a>

		</div>
	</div>

</div>