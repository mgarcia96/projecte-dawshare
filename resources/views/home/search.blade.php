@extends('layouts.base')

@section('content')
	
	<div class="col-md-10 col-md-offset-1">
		
		{{-- BUSCADOR --}}
		<div class="well well-sm">
		{!! Form::open(['url' => url('/search'), 'method' => 'get']) !!}
			<div class="form-group label-floating has-info mt-10">
			  <div class="input-group">
			    <label class="control-label" for="text">Buscador de usuaris i posts...</label>
			    <input autocomplete="off" type="text" name="text" id="text" class="form-control" value="{{ request()->get('text') }}">
			    <span class="input-group-btn">
			      <button type="submit" class="btn btn-fab btn-info btn-fab-mini">
			        <i class="material-icons">search</i>
			      </button>
			    </span>
			  </div>
			</div>
		{!! Form::close() !!}
		</div>
		{{-- END BUSCADOR --}}

		<div role="tabpanel">
			<!-- Nav tabs -->
			<ul class="nav nav-tabs background-menu" role="tablist">
				<li role="presentation" class="active">
					<a href="#users" aria-controls="users" role="tab" data-toggle="tab">Usuarios</a>
				</li>
				<li role="presentation">
					<a href="#posts" aria-controls="posts" role="tab" data-toggle="tab">Posts</a>
				</li>
			</ul>
		
			<!-- Tab panes -->
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="users"><br>
					@if(count($users) < 1)
						<h2>No s'han trobat usuaris amb la paraula "{{ request()->get('text') }}"</h2>
					@endif

					@foreach($users as $pos => $user)
						
					<a href="{{ url('/user/'.$user->url) }}">
						<div class="col-md-6 {{ $pos % 2 == 0 ? 'no-padding-left' : 'no-padding-right' }}">
							<div class="well well-sm user-search text-info">
								<div class="row">
									<div>
										<img src="{{ asset('img/users/'.$user->profile_image) }}" class="img-responsive" alt="">					
										{{ $user->name }}
									</div>
								</div>
							</div>
						</div>
					</a>

					@endforeach

				</div>
				<div role="tabpanel" class="tab-pane" id="posts"><br>

					@if(count($posts) < 1)
						<h2>No s'han trobat posts amb la paraula "{{ request()->get('text') }}"</h2>
					@endif

					{{-- USERS TAB --}}
					@foreach($posts as $post)
			
						<article class="well well-sm post-card">

							<div class="profile-header">
								@if(isset($post->user))
									<img src="{{ asset('/img/users/'.$post->user['profile_image']) }}" alt="">
									<a class="text-info" href="{{ url('/user/'.$post->user['url']) }}">{{ $post->user['name'] }}</a>
								@elseif(isset($post->page))
									<img src="{{ asset('/img/pages/'.$post->page['profile_image']) }}" alt="">
									<a class="text-info" href="{{ url('/page/'.$post->page['url']) }}">{{ $post->page['name'] }}</a>
								@endif

								<span class="pull-right date">
									{{ Carbon\Carbon::parse($post->created_at)->diffForHumans() }}
								</span>
							</div>

							<div class="header">
								@if(isset($post->image) || isset($post->gif))
									<img src="{{ asset('/img/users/'.$post->user['url'].'/posts/'.$post->image) }}" class="img-responsive" alt="">
								@elseif(isset($post->video))
									<video controls>
										<source src="{{ asset('/img/users/'.$post->user['url'].'/posts/'.$post->video) }}" type="video/mp4"></source>
									</video>
								@endif
							
								<div class="header-media">
									{{-- LIKE BUTTON --}}
									<button class="btn btn-fab btn-float like-button {{ isset(Auth::user()->post_likes) ? in_array($post->id, Auth::user()->post_likes) ? 'btn-info' : '' : '' }}" data-post_id="{{ $post->id }}" data-status="{{ isset(Auth::user()->post_likes) ? in_array($post->id, Auth::user()->post_likes) ? 'true' : 'false' : '' }}">
										<i class="material-icons" title="Like +1">thumb_up</i>
									</button>

									{{-- COMMENTS BUTTON --}}

									<button class="btn btn-fab btn-float-right view_comments">
										<i class="material-icons" title="Veure Comentaris">question_answer</i>
									</button>
								</div>
							</div>

							<div class="content">
								
								<p class="text-info">
									A <b class="likes-number">{{ $post->likes }}</b> <b>{{ $post->likes == 1 ? 'persona' : 'persones' }}</b> els hi agrada aquest post.

									<span class="pull-right">
										<b class="comments-number">{{ count($post->comments) }}</b> comentaris
									</span>
								</p>

								<hr>
								<p>{{ $post->post }}</p>
								<hr>
							</div>

							<div class="comments">
								{!! Form::open(['id' => 'post-comment']) !!}
									{!! Form::hidden('post_id', $post->id) !!}
									
									<div class="form-group label-floating mt-10">
										<div class="input-group">
											<label class="control-label" for="post">Escriu un comentari...</label>
											<input type="text" id="comment" name="comment" value="{{ old('post') }}" id="post" class="form-control " rows="2">
											<span class="input-group-btn">
											  <button type="button" id="comment-submit" class="btn btn-info btn-fab btn-fab-mini" title="Enviar comentari">
											    <i class="fa fa-comments-o"></i>
											  </button>
											</span>
										</div>
									</div>

								{!! Form::close() !!}

								{{-- Comments list --}}

								<div class="comments_box">
								
									@if(isset($post->comments))
										@foreach($post->comments as $comment)
											<div class="comment">
												<a class="text-info" href="{{ url('/user/'.$comment['user_url']) }}">
													<img src="{{ asset('/img/users/'.$comment['user_profile_image']) }}" alt="">
													{{ $comment['user_name'] }}
												</a>
												<span class="pull-right date">
													{{ Carbon\Carbon::parse($comment['created_at']['date'])->diffForHumans() }}
												</span>
												<p class="comment-text">
													{{ $comment['comment'] }}
												</p>
											</div>
										@endforeach
									@endif

								</div>

								{{-- END Comments list --}}
							</div>
						</article>

					@endforeach
					{{-- END USERS TAB --}}
					
				</div>
			</div>
		</div>

	</div>

@endsection

@section('css')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ladda-bootstrap/0.9.4/ladda-themeless.min.css">
@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/ladda-bootstrap/0.9.4/spin.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ladda-bootstrap/0.9.4/ladda.min.js"></script>
<script>
$(function() {

	// View Comments button
	$(".view_comments").click(function(event) {
		$(this).parents(".post-card").find(".comments_box").slideToggle();
	});

	// POST LIKE
	$(".like-button").click(function(event) {
		var card = $(this).parents('.post-card');
		var button = $(this);
		$.ajax({
			url: '{{ url('/ajax/like-post') }}',
			type: 'post',
			data: {
				'_token': '{{ csrf_token() }}',
				'post_id': $(this).data('post_id'),
				'status': $(this).data('status'),
			},
		})
		.done(function(msg) {
			var likes = parseInt(card.find('.likes-number').text());

			if(msg == 'true') {
				likes++;
				button.addClass('btn-info');
				card.find('.likes-number').text(likes);
			} else {
				likes--;
				button.removeClass('btn-info');
				card.find('.likes-number').text(likes);
			}
		});
		
	});

	// POST COMMENT
	$("#post-comment #comment-submit").click(function(event) {
		var form = $(this).parents('#post-comment');
		var card = $(this).parents('.post-card');

		if(form.find("#comment").val() == '')
			return false;

		$.ajax({
			url: '{{ url('/ajax/new-comment') }}',
			type: 'post',
			data: {
				'_token': '{{ csrf_token() }}',
				'comment': form.find("#comment").val(),
				'post_id': form.find("input[name='post_id']").val()
			},
		})
		.done(function(msg) {
			var html = '<div class="comment" display="none">\
							<a class="text-info" href="http://projecte.app/user/'+msg.user_url+'">\
								<img src="http://projecte.app/img/users/'+msg.user_profile_image+'" alt="">\
								'+msg.user_name+'\
							</a>\
							<span class="pull-right date">\
								'+msg.human_date+'\
							</span>\
							<p class="comment-text">\
								'+msg.comment+'\
							</p>\
						</div>';

			var comment_box = form.parents(".comments");
			comment_box.find(".comments_box").prepend(html);
			comment_box.find(".comments_box").slideDown();
			comment_box.find(".comments_box").first(".comment").slideDown();

			comment_box.find("#comment").val("");

			var number = parseInt(card.find('.comments-number').text());
			number++;
			card.find('.comments-number').text(number);
		});
		
	});

	$('[data-toggle="tooltip"]').tooltip();
	$(".comments_box").hide();
	
});
</script>
@endsection