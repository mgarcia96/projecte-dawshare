@extends('layouts.base-no-menu')

@section('content')
	
	<div class="welcome-background-video">
		<video autoplay="" loop>
			<source src="{{ asset('assets/videos/welcome.mp4') }}" type="video/mp4">
		</video>
	</div>

	<div class="welcome-video-overlay">
		
		<div class="welcome-login-box">
			<div class="col-md-4 col-md-offset-4	">
				
				<div class="well well-lg login-register-box row">
					
					{{-- LOGIN BOX --}}
					<div class="col-md-12 login-box">
						
						<h3 class="text-center">
							- Iniciar Sessió -
							<hr>
						</h3>

						@if(Session::has('error_login'))
							<div class="alert alert-danger">
								{{ Session::get('error_login') }}
							</div>
						@endif

						<form method="POST" action="/auth/login">
						    {!! csrf_field() !!}

						    <div class="form-group">
						        Email
						        <input class="form-control" type="email" name="email" value="{{ old('email') }}">
						    </div>

						    <div class="form-group">
						        Password
						        <input class="form-control" type="password" name="password" id="password">
						    </div>

						    <div class="form-group">
						    	<div class="togglebutton">
				              		<label>
					                	Recorda'm 
					                	<input checked="" name="remember" type="checkbox">
				              		</label>
				            		<button type="submit" class="btn btn-primary btn-raised pull-right no-margin">Iniciar Sessió</button>
					            </div>

						    </div>
						</form>

						<hr>


						<div class="text-center">
							<p>- o -</p>
							<button class="btn btn-raised btn-info btn-lg js-register-show-button" data-toggle="modal" href='#register-modal'>Crear un nou compte</button>
						</div>

						@if (count($errors) > 0)
					    <div class="alert alert-danger">
					        <ul>
					            @foreach ($errors->all() as $error)
					                <li>{{ $error }}</li>
					            @endforeach
					        </ul>
					    </div>
					    @endif
					</div>

				</div>

			</div>
		</div>

	</div>

<div class="modal fade" id="register-modal">
	<div class="modal-dialog">
		<div class="modal-content">

			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">- Registrar-se -</h4>
			</div>

			<form method="POST" class="register-box row" action="/auth/register">
			<div class="modal-body">
				{{-- REGISTER BOX --}}
					
				    @if (count($errors) > 0)
				    <div class="alert alert-danger">
				        <ul>
				            @foreach ($errors->all() as $error)
				                <li>{{ $error }}</li>
				            @endforeach
				        </ul>
				    </div>
				    @endif

				    {!! csrf_field() !!}

				    <div class="form-group">
				        Name
				        <input class="form-control" type="text" name="name" value="{{ old('name') }}">
				    </div>

				    <div class="form-group">
				        Email
				        <input class="form-control" type="email" name="email" value="{{ old('email') }}">
				    </div>

				    <div class="form-group">
				        Password
				        <input class="form-control" type="password" name="password">
				    </div>

				    <div class="form-group">
				        Confirm Password
				        <input class="form-control" type="password" name="password_confirmation">
				    </div>


				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Tancar</button>
					<button type="submit" class="btn btn-info btn-raised">Registrar-se</button>
				</div>
				</form>
			</div>
		</div>
	</div>
</div>

@stop

@section('js')

@endsection