<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;

use App\User;
use App\Models\Post;

class HomeController extends Controller
{
    public function getIndex() {
    	if(isset(Auth::user()->friends))
    		$data['posts'] = Post::whereIn('user.url', Auth::user()->friends)->orderBy('created_at', 'DESC')->get();
    	else
    		$data['posts'] = Post::orderBy('created_at', 'DESC')->get();
    	
        return view('home.index', $data);
    }

    public function getSearch() {
    	$text = request()->get('text');

    	$data = [
    		'posts' => Post::where('post', 'LIKE', '%'. $text . '%')->get(),
    		'users' => User::where('name', 'LIKE', '%'. $text . '%')->get(),
    	];

    	return view('home.search', $data);
    }

}