<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Post;
use Auth;

class PostController extends Controller
{
    public function postNewPost() {

    	$validator = \Validator::make(request()->all(), [
    		'post' => 'required|max:1000'
    	]);

    	if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }
        
    	$post = new Post();

    	$post->post = request()->get('post');

        $post->user = new \stdClass();
    	$post->user->name = Auth::user()->name;
    	$post->user->url = Auth::user()->url;
    	$post->user->profile_image = Auth::user()->profile_image;

        $validator = true;

        // IMAGE UPLOAD
        if(request()->hasFile('image') && request()->file('image')->isValid()) {
            $image = request()->file('image');
            $image_name = str_replace(' ', '-', $image->getClientOriginalName());
            // jpg, png, jpeg. Màx. 500KB
            if($image->getSize() <= 512000 && ($image->getClientOriginalExtension() == 'png' || $image->getClientOriginalExtension() == 'jpg' || $image->getClientOriginalExtension() == 'jpeg')) {
                $image->move(public_path('/img/users/'.Auth::user()->url.'/posts/'), $image_name);

                $post->image = $image_name;
            } else
                $validator = false;            

        }

        // video UPLOAD
        if(request()->hasFile('video') && request()->file('video')->isValid()) {
            $video = request()->file('video');
            $video_name = str_replace(' ', '-', $video->getClientOriginalName());
            // jpg, png, jpeg. Màx. 8MB
            if($video->getSize() <= 8388608 && ($video->getClientOriginalExtension() == 'mp4')) {
                $video->move(public_path('/img/users/'.Auth::user()->url.'/posts/'), $video_name);

                $post->video = $video_name;
            } else
                $validator = false;            

        }

        // gif UPLOAD
        if(request()->hasFile('gif') && request()->file('gif')->isValid()) {
            $gif = request()->file('gif');
            $gif_name = str_replace(' ', '-', $gif->getClientOriginalName());
            // .gif. Màx. 500KB
            if($gif->getSize() <= 512000 && ($gif->getClientOriginalExtension() == 'gif')) {
                $gif->move(public_path('/img/users/'.Auth::user()->url.'/posts/'), $gif_name);

                $post->gif = $gif_name;
            } else
                $validator = false;            

        }

        if($validator) {
            $post->save();
            return redirect()->back()->with(['msg' => 'Post creat correctament', 'type' => 'success']);
        } else {
            return redirect()->back()->withInput()->with(['msg' => 'Format incorrecte', 'type' => 'error']);
        }


    }

}