<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use App\Models\Post;

class UserController extends Controller
{
    /**
     * @return Users list
     * */
    public function postIndex() {
        $data['users'] = User::all();

        return response()->json([$data, 200]);
    }

    /**
     * @var user_id
     * @return User detail
     * */
    public function postDetail() {
    	$user_id = request()->get('user_id');

    	if($user_id == '')
    		return response()->json([['status' => 'error', 'msg' => 'user_id necessari'], 403]);

    	$data['user'] = User::findOrFail($user_id);

        return response()->json([$data, 200]);
    }

    public function postUserPosts() {
    	$user_id = request()->get('user_id');

    	if($user_id == '')
    		return response()->json([['status' => 'error', 'msg' => 'user_id necessari'], 403]);

    	$user = User::findOrFail($user_id);

    	$data['posts'] = Post::where('user.url', $user->url)->get();

        return response()->json([$data, 200]);
    }

}
