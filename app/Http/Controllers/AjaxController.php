<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;

use App\User;
use App\Models\Post;

class AjaxController extends Controller
{
    public function postAddFriend() {
        $friend = User::findOrFail(request()->get('user'));

        Auth::user()->addFriend($friend);

        return 'OK';
    }

    public function postRemoveFriend() {
        $friend = User::findOrFail(request()->get('user'));

        Auth::user()->removeFriend($friend);

        return 'OK';
    }

    /**
     * @return liked or unliked status
     * */
    public function postLikePost() {
        $post = Post::findOrFail(request()->get('post_id'));

        $status = "false";

        if(!isset($post->likes))
            $post->likes = 1;

        $user = User::find(Auth::user()->id);

        if(!isset($user->post_likes))
            $user->post_likes = [];

        $post_likes = $user->post_likes;

        if(!in_array($post->id, $post_likes)) {
            array_push($post_likes, $post->id);
            $status = "true";
            $post->likes++;
        }
        else {
            $key = array_search($post->id, $post_likes);
            unset($post_likes[$key]);
            $status = "false";
            $post->likes--;
        }

        $user->post_likes = $post_likes;

        $post->save();
        $user->save();

        return $status;
    }

    public function postNewComment() {
    	$post = Post::findOrFail(request()->get('post_id'));

    	if(!isset($post->comments))
    		$post->comments = [];

    	$newcomment = [
    		'user_name' => Auth::user()->name,
    		'user_url' => Auth::user()->url,
    		'user_profile_image' => Auth::user()->profile_image,
    		'comment' => request()->get('comment'),
    		'created_at' => \Carbon\Carbon::now(),
            'human_date' => '',
    	];

    	$comments = $post->comments;

    	array_unshift($comments, $newcomment);

    	$post->comments = $comments;

    	$post->save();

        // Human date
        $newcomment['human_date'] = $newcomment['created_at']->diffForHumans();

    	return $newcomment;
   
   }

}