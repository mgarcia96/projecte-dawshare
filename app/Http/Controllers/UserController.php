<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;

use App\User;

class UserController extends Controller
{
    /**
     * getProfile()
     * @var $url => String
     * @return view => profile with data user
     * */
    public function getProfile($url) {
        if($url == Auth::user()->url)
            $user = User::find(Auth::user()->id);
        else
            $user = User::where('url', $url)->first();

        if(!$user)
            return abort(404);

        $data['user'] = $user;
        $data['posts'] = $user->getPosts();
        return view('profile.me', $data);

    }
}
