<?php

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

// API Routes
Route::group(['prefix' => 'api', 'middleware' => 'api'], function() {
    Route::controller('/users', 'Api\UserController');
});

// Rutes amb sessió iniciada
Route::group(['middleware' => 'auth'], function() {
	Route::get('/user/{url}', 'UserController@getProfile');
	Route::controller('/user', 'UserController');
    Route::controller('/ajax', 'AjaxController');
    Route::controller('/post', 'PostController');
    Route::controller('/', 'HomeController');
});