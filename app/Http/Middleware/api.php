<?php

namespace App\Http\Middleware;

use Closure;

class api
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->get('token') == env('api_token'))
            return $next($request);
        else
            return abort(403, 'Error api token');
    }
}
