<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Jenssegers\Mongodb\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use App\Models\Post;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password', 'profile_image'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function countPosts() {
        return Post::where('user.url', $this->url)->count();
    }

    public function getPosts() {
        return Post::where('user.url', $this->url);
    }

    public function addFriend($friend) {

        if(!isset($this->friends))
            $this->friends = [];

        $friends = $this->friends;

        array_push($friends, $friend->url);

        $this->friends = $friends;
        $this->save();
    }

    public function removeFriend($friend) {

        if(!isset($this->friends))
            $this->friends = [];

        $friends = $this->friends;

        array_unshift($friends, $friend->url);
        $this->friends = $friends;
        $this->save();
    }
}